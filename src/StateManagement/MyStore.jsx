import CandidateRegInfo from './AppSlices/RegDataSliice';
import {configureStore} from '@reduxjs/toolkit';
// import {combineReducers} from '@reduxjs/toolkit';

// const CandidateData=combineReducers()


const  MyStore=configureStore({reducer:{UserRegData:CandidateRegInfo}});

export default MyStore;

