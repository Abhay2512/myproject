import { createSlice } from '@reduxjs/toolkit';

const RegDataSlice = createSlice({
    name: 'UserRegData',
    initialState: [] ,
    reducers: {  
        userAccountOpen:(state,action)=>{
            state.push(action.payload);
        }
    }

})

export const {userAccountOpen} = RegDataSlice.actions;

export default RegDataSlice.reducer;
